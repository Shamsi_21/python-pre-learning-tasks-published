def factors(number):
    # ==============
    result = list()
    for i in range(1, int(number ** 0.5) + 1):
        div, mod = divmod(number, i)
        if mod == 0:
            result.append(i)
            result.append(div)
    del result[0:2]
    result.sort()
    return result
    # ==============


print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “[]” (an empty list) to the console
